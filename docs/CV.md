# CV konfiguracyjne

| CV | Zakres | Domyślna | Opis |
| :---: | :---: | :---: | :---: |
| `cv` | `string` | `null` | Ścieżka do pliku CV. |
| `cv` | `string[]` | `null` | Tablica ścieżek do plików CV. |